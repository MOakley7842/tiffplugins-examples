# README #

This contains two examples one for converting PDF to Tiff Fax using ICEPdf.  The second started with the current Apache Tiff to PDF Converter with the goal being to reduce the output file size of the PDF when the source Tiff is a black and white image.  This update has reduced the PDF file size for 3pagetiff.tif from 1.4mb to 166kb.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

    <repositories>
        <repository>
            <id>icePdfAPI</id>
            <name>icePdf API</name>
            <url>http://anonsvn.icesoft.org/repo/maven2/releases/</url>
        </repository>
    </repositories>

        <dependency>
            <groupId>org.icepdf</groupId>
            <artifactId>icepdf-core</artifactId>
            <version>5.0.5</version>
        </dependency>

       <dependency>
            <groupId>javax.media</groupId>
            <artifactId>jai_core</artifactId>
            <version>1.1.3</version>
        </dependency>
        <dependency>
            <groupId>com.sun.media</groupId>
            <artifactId>jai_imageio</artifactId>
            <version>1.1</version>
        </dependency>


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact