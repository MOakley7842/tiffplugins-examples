package com.examples.tiffplugins;


//import com.sun.media.imageio.plugins.tiff.TIFFDirectory;
//import com.sun.media.imageio.plugins.tiff.TIFFField;
import com.sun.media.jai.codec.*;
import com.sun.media.jai.codec.TIFFDirectory;
import com.sun.media.jai.codec.TIFFField;
import com.sun.media.jai.codec.SeekableStream;

import org.apache.commons.io.FileUtils;
import com.google.common.io.Files;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.TIFFDecodeParam;

import javax.imageio.*;
import javax.media.jai.JAI;
import javax.media.jai.RenderedOp;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import javax.imageio.stream.ImageOutputStream;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.ParameterBlock;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;


/**
 * Created by moakley on 4/29/15.
 */
public class TiffToPdfConverter {

    /*
    public void convert(InputStream inputStream, OutputStream outputStream) {
        Iterator readersIterator = ImageIO.getImageReadersByFormatName("tiff");
        com.sun.media.imageioimpl.plugins.tiff.TIFFImageReader imageReader;
        ImageInputStream imageInputStream;
        TIFFDecodeParam param = new TIFFDecodeParam();

        try {
            if (readersIterator.hasNext())
                imageReader = (com.sun.media.imageioimpl.plugins.tiff.TIFFImageReader)readersIterator.next();
            else
                throw new IOException("No image reader available for tif");

            imageReader.setInput(ImageIO.createImageInputStream((FileInputStream)inputStream));


            SeekableStream seekableStream = SeekableStream.wrapInputStream(inputStream, false);

            ImageDecoder imageDecoder = ImageCodec.createImageDecoder("tiff", inputStream, param);
            int pageCount = imageDecoder.getNumPages();

            for (int page = 0; page < pageCount; page++){
                IIOMetadata imageMetadata = imageReader.getImageMetadata(page);
                com.sun.media.imageio.plugins.tiff.TIFFDirectory ifd =
                        com.sun.media.imageio.plugins.tiff.TIFFDirectory.createFromMetadata(imageMetadata);

                int compression = getCompression(ifd);
                int bitsPerSample = getBitsPerSample(ifd);

            }




        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }


    static int TAG_COMPRESSION = 259;
    public static int getCompression(com.sun.media.imageio.plugins.tiff.TIFFDirectory ifd) {
        com.sun.media.imageio.plugins.tiff.TIFFField tiffField = ifd.getTIFFField(TAG_COMPRESSION);
        int valueOfField = tiffField.getAsInt(0);
        return valueOfField;
    }

    static int TAG_BITSPERSAMPLE = 258;
    public static int getBitsPerSample(com.sun.media.imageio.plugins.tiff.TIFFDirectory ifd) {
        com.sun.media.imageio.plugins.tiff.TIFFField tiffField = ifd.getTIFFField(TAG_BITSPERSAMPLE);
        int valueOfField = tiffField.getAsInt(0);
        return valueOfField;
    }
*/
/*
    public void convert(InputStream inputStream, OutputStream outputStream) {
        ImageInputStream imageInputStream;

        try {
            SeekableStream seekableStream = SeekableStream.wrapInputStream(inputStream, true);
            ParameterBlock pb = new ParameterBlock();
            pb.add(seekableStream);

            TIFFDecodeParam param = new TIFFDecodeParam();
            pb.add(param);

            java.util.ArrayList images = new ArrayList();

            long nextOffset = 0;
            int page = 0;

            do {
                //javax.media.jai.RenderedOp renderOp = JAI.create("tiff", pb);
                javax.media.jai.RenderedOp renderedOp = javax.media.jai.JAI.create("tiff", pb);
                images.add(renderedOp);
                TIFFDirectory ifd = (TIFFDirectory)renderedOp.getProperty("tiff_directory");


                int compression = getTiffCompression(ifd);
                int bitsPerSample = getTiffBitsPerSample(ifd);
                System.out.println(nextOffset + ", " + compression + ", " + bitsPerSample + ", " + page);

                nextOffset = ifd.getNextIFDOffset();
                if (nextOffset != 0) {
                    param.setIFDOffset(nextOffset);
                    page++;
                }

            } while(nextOffset != 0);

        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }


    static int TAG_COMPRESSION = 259;
    public static int getTiffCompression(TIFFDirectory ifd) {
        TIFFField tiffField = ifd.getField(TAG_COMPRESSION);
        int valueOfField = tiffField.getAsInt(0);
        return valueOfField;
    }

    static int TAG_BITSPERSAMPLE = 258;
    public static int getTiffBitsPerSample(TIFFDirectory ifd) {
        TIFFField tiffField = ifd.getField(TAG_BITSPERSAMPLE);
        int valueOfField = tiffField.getAsInt(0);
        return valueOfField;
    }
*/
    public void convert(InputStream inputStream, OutputStream outputStream) throws IOException {

        TIFFDecodeParam param = null;
        final String rootPath = Files.createTempDir().getPath();
        ArrayList<String> imageFiles = new ArrayList<>();
        PDDocument pdfDocument;


        try {

            Iterator<ImageWriter> imageJpegWriters = ImageIO.getImageWritersBySuffix("jpeg");

            if (!imageJpegWriters.hasNext()){
                throw new IllegalStateException("no jpeg image writers found");
            }

            ImageWriter imageJpegWriter = imageJpegWriters.next();

            Iterator<ImageWriter> imagePngWriters = ImageIO.getImageWritersBySuffix("png");

            if (!imagePngWriters.hasNext()){
                throw new IllegalStateException("no png image writers found");
            }

            ImageWriter imagePngWriter = imagePngWriters.next();

            ImageDecoder imageDecoder = ImageCodec.createImageDecoder("tiff", inputStream, param);

            String imageFilename;

            for (int x = 0; x < imageDecoder.getNumPages(); x++) {

                RenderedImage image = imageDecoder.decodeAsRenderedImage(x);

                int bitsPerPixel = getBitsPerPixel(imageDecoder.getInputStream());

                if (bitsPerPixel == 1) {
                    imageFilename = String.format(rootPath + "/" +  UUID.randomUUID().toString() + ".png");

                    writePngImage(imagePngWriter, image, imageFilename);
                }
                else {
                    imageFilename = String.format(rootPath + "/" + UUID.randomUUID().toString() + ".jpeg");

                    writeJpegImage(imageJpegWriter, image, imageFilename);
                }

                imageFiles.add(imageFilename);
            }

            pdfDocument = new PDDocument();
            PDXObjectImage pdxImage;

            for (String imageFile : imageFiles) {

                if (imageFile.endsWith("png")){
                    BufferedImage awtImage = ImageIO.read( new File( imageFile ) );
                    pdxImage = new PDPixelMap(pdfDocument, awtImage);
                } else {
                    pdxImage = new PDJpeg(pdfDocument,
                            new FileInputStream(imageFile));
                }

                PDPage page = new PDPage(
                        new PDRectangle(pdxImage.getWidth(), pdxImage.getHeight()));

                pdfDocument.addPage(page);
                PDPageContentStream contentStream = new PDPageContentStream(pdfDocument, page, true, true);
                contentStream.drawImage(pdxImage, 0, 0);
                contentStream.close();
            }

            final int numPages = pdfDocument.getNumberOfPages();


            pdfDocument.save(outputStream);
            pdfDocument.close();

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            for (String tempFile : imageFiles) {
                remove(rootPath);
            }
        }
    }

    private int getBitsPerPixel(SeekableStream seekableStream) throws Exception {
        int bitsPerPixel = 1;
        try {
            TIFFDirectory ifd = new TIFFDirectory(seekableStream, 0);
            bitsPerPixel = getTiffBitsPerSample(ifd);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }

        return bitsPerPixel;
    }

    static int TAG_BITSPERSAMPLE = 258;
    public static int getTiffBitsPerSample(TIFFDirectory ifd) {
        TIFFField tiffField = ifd.getField(TAG_BITSPERSAMPLE);
        int valueOfField = tiffField.getAsInt(0);
        return valueOfField;
    }

    private void remove(String rootPath){

        try
        {
            FileUtils.cleanDirectory(new File(rootPath));
            FileUtils.deleteDirectory(new File(rootPath));

        } catch (IOException e) {
            e.printStackTrace();
        }
        catch(IllegalArgumentException e){}
    }


    private void writeJpegImage(ImageWriter imageWriter, RenderedImage image, String imageFile) throws IOException{

        File tempFile = new File(imageFile);
        ImageOutputStream imageOutputStream = javax.imageio.ImageIO.createImageOutputStream(tempFile);
        try
        {
            ImageWriteParam imageWriteParam = imageWriter.getDefaultWriteParam();
            imageWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            imageWriteParam.setCompressionQuality(0.3f);
            imageWriter.setOutput(imageOutputStream);
            imageWriter.write(null, new IIOImage(image, null, null), imageWriteParam);
        }
        catch(IOException ex){
            ex.printStackTrace();
            throw ex;
        }
        finally{
            if (imageOutputStream != null){
                imageOutputStream.flush();
                imageOutputStream.close();
            }
        }
    }

    private void writePngImage(ImageWriter imageWriter, RenderedImage image, String imageFile) throws IOException{

        File tempFile = new File(imageFile);
        ImageOutputStream imageOutputStream = javax.imageio.ImageIO.createImageOutputStream(tempFile);
        try
        {
            ImageWriteParam imageWriteParam = imageWriter.getDefaultWriteParam();
            //imageWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            //imageWriteParam.setCompressionQuality(0.3f);
            imageWriteParam.setProgressiveMode( javax.imageio.ImageWriteParam.MODE_COPY_FROM_METADATA );
            imageWriteParam.setDestinationType(
                    new ImageTypeSpecifier(image.getColorModel(), image.getSampleModel() ) );
            imageWriter.setOutput(imageOutputStream);
            imageWriter.write(null, new IIOImage(image, null, null), imageWriteParam);
        }
        catch(IOException ex){
            ex.printStackTrace();
            throw ex;
        }
        finally{
            if (imageOutputStream != null){
                imageOutputStream.flush();
                imageOutputStream.close();
            }
        }
    }


    public void readTiffImageProperties(String inputTifImagePath) {
        Iterator readersIterator = ImageIO.getImageReadersByFormatName("tif");
        ImageReader imageReader = (ImageReader) readersIterator.next();
        ImageInputStream imageInputStream;

        try {
            System.out.println("Filename:  " + inputTifImagePath);
            imageInputStream = new FileImageInputStream(new File(inputTifImagePath));
            imageReader.setInput(imageInputStream, false, true);
            FileSeekableStream fileSeekableStream;
            fileSeekableStream = new FileSeekableStream(inputTifImagePath);
            ImageDecoder iDecoder = ImageCodec.createImageDecoder("tiff", fileSeekableStream, null);
            int pageCount = iDecoder.getNumPages();

            for (int page = 0; page < pageCount; page++) {
                IIOMetadata imageMetadata = imageReader.getImageMetadata(page);
/*
* The root of all the tags for this image is the IFD (Image File Directory).
* Get the IFD from where we can get all the tags for the image.
*/
                com.sun.media.imageio.plugins.tiff.TIFFDirectory ifd =
                        com.sun.media.imageio.plugins.tiff.TIFFDirectory.createFromMetadata(imageMetadata);
                com.sun.media.imageio.plugins.tiff.TIFFField[] allTiffFields = ifd.getTIFFFields();

                for (int i = 0; i < allTiffFields.length; i++) {
                    com.sun.media.imageio.plugins.tiff.TIFFField tiffField = allTiffFields[i];
                    String nameOfField = tiffField.getTag().getName();
                    int numberOfField = tiffField.getTagNumber();
                    String typeOfField = com.sun.media.imageio.plugins.tiff.TIFFField.getTypeName(tiffField.getType());
                    String valueOfField = tiffField.getValueAsString(0);
                    System.out.println((i + 1) + ". " + nameOfField + ", " + numberOfField + ", " + typeOfField + ", " + valueOfField);
                }
                System.out.println("======================================");
            }
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

}
