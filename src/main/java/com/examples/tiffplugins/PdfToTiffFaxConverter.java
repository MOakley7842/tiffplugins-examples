package com.examples.tiffplugins;


import com.google.common.io.Files;
import com.sun.media.imageio.plugins.tiff.BaselineTIFFTagSet;
import org.icepdf.core.exceptions.PDFException;
import org.icepdf.core.exceptions.PDFSecurityException;
import org.icepdf.core.pobjects.Document;
import org.icepdf.core.pobjects.PDimension;
import org.icepdf.core.pobjects.Page;
import org.icepdf.core.util.GraphicsRenderingHints;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.IndexColorModel;
import java.io.*;
import java.util.Iterator;
import java.util.UUID;

/**
 * Created by moakley on 6/12/15.
 */
public class PdfToTiffFaxConverter {

    public static final int DEFAULT_FAX_DPI = 200;
    public static final int DEFAULT_FAX_COLOR = BufferedImage.TYPE_BYTE_BINARY;
    public static final int DEFAULT_FAX_COMPRESSION = BaselineTIFFTagSet.COMPRESSION_CCITT_T_6;
    public static final String DEFAULT_FAX_COMPRESSION_TYPE = "CCITT T.6";
    public static final float DEFAULT_FAX_COMPRESSION_QUALITY = 1.0f;

    public void convert(InputStream inputStream, OutputStream outputStream) throws IOException {
        final String rootPath = Files.createTempDir().getPath();

        try {
            String tempPdfFile = rootPath + "/" + UUID.randomUUID().toString() + ".pdf";
            Document document = new Document();
            document.setInputStream(inputStream, tempPdfFile);

            Iterator<ImageWriter> imageTiffWriters = ImageIO.getImageWritersBySuffix("tiff");
            if (!imageTiffWriters.hasNext()){
                throw new IllegalStateException("no tiff image writers found");
            }
            ImageWriter imageWriter = imageTiffWriters.next();

            ImageOutputStream imageOutputStream = ImageIO.createImageOutputStream(outputStream);
            imageWriter.setOutput(imageOutputStream);

            for (int i = 0; i < document.getNumberOfPages(); i++) {
                final double targetDPI = DEFAULT_FAX_DPI;
                float scale = DEFAULT_FAX_COMPRESSION_QUALITY;
                float rotation = 0f;
                PDimension size = document.getPageDimension(i, rotation, scale);
                double dpi = Math.sqrt((size.getWidth()*size.getWidth()) + (size.getHeight()*size.getHeight()) )
                        /Math.sqrt((8.5*8.5)+(11*11));

                if (dpi < (targetDPI-0.1)) {
                    scale = (float) (targetDPI / dpi);
                    size = document.getPageDimension(i, rotation, scale);
                }

                int pageWidth = (int) size.getWidth();
                int pageHeight = (int) size.getHeight();
                int[] cmap = new int[] { 0xFF000000, 0xFFFFFFFF };
                IndexColorModel cm = new IndexColorModel(1, cmap.length, cmap, 0, false, Transparency.OPAQUE, DataBuffer.TYPE_BYTE);
                BufferedImage image = new BufferedImage(pageWidth, pageHeight, DEFAULT_FAX_COLOR, cm);

                Graphics g = image.createGraphics();
                document.paintPage(i, g, GraphicsRenderingHints.PRINT, Page.BOUNDARY_CROPBOX, rotation, scale);
                g.dispose();
                IIOImage img = new IIOImage(image, null, null);
                ImageWriteParam param = imageWriter.getDefaultWriteParam();
                param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                param.setCompressionType(DEFAULT_FAX_COMPRESSION_TYPE);
                if (i == 0) {
                    imageWriter.write(null, img, param);
                } else {
                    imageWriter.writeInsert(-1, img, param);
                }

                image.flush();
            }

            imageOutputStream.flush();
            imageOutputStream.close();
            imageWriter.dispose();
            System.out.println("PDF converted to Tiff successfully with compression type " + DEFAULT_FAX_COMPRESSION);
        }
        catch(PDFException ex) {
            System.out.println("Error parsing PDF document " + ex);
        } catch(PDFSecurityException ex) {
            System.out.println("Error encryption not supported " + ex);
        } catch(IOException ex) {
            System.out.println("Error handling PDF document " + ex);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
