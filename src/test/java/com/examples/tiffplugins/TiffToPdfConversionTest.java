package com.examples.tiffplugins;


import com.examples.tiffplugins.TiffToPdfConversionTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Arrays;
import java.util.Collection;



/**
 * Created by moakley on 4/29/15.
 */
@RunWith(Parameterized.class)
public class TiffToPdfConversionTest {

    @Parameterized.Parameters
    public static Collection sourceFiles() {
        return Arrays.asList(new Object[][] {
                {new TiffToPdfConverter()}
        });
    }

    protected String sourceFile;
    protected String outputFile;
    protected TiffToPdfConverter tiffToPdfConverter;
    protected OutputStream outputStream;
    protected InputStream inputStream;

    public TiffToPdfConversionTest(TiffToPdfConverter converter)
    {
        String url = TiffToPdfConversionTest.class.getResource("/test/image/").getPath();
        // java.io.IOException: Unsupported combination of photometric interpretation, samples per pixel, and bit depth.
        //this.sourceFile = url + "LZW_Pallette_1Bit.tif";
        //this.outputFile = url + "LZW_Pallette_1Bit.pdf";
        // java.io.IOException: Decoding of old style JPEG-in-TIFF data is not supported.
        //this.sourceFile = url + "JpegOrigTiff60_YCbCr.tif";
        //this.outputFile = url + "JpegOrigTiff60_YCbCr.pdf";
        // Page one is really skewed right, shift right and wraps on left
        //this.sourceFile = url + "JpegTechnote2_MinIsBlack_Page2.tif";
        //this.outputFile = url + "JpegTechnote2_MinIsBlack_Page2.pdf";
        // Page one is really skewed right, shift right and wraps on left
        //this.sourceFile = url + "3pagetiff.tif";
        //this.outputFile = url + "3pagetiff.pdf";
        // Looks fine
        //this.sourceFile = url + "FaxTest.tif";
        //this.outputFile = url + "FaxTest.pdf";
        // Looks fine
        this.sourceFile = url + "AASF.tif";
        this.outputFile = url + "AASF.pdf";
        this.tiffToPdfConverter = converter;
    }

    @Before
    public void setup() throws FileNotFoundException {
        outputStream = new ByteArrayOutputStream();
    }

    @After
    public void tearDown() throws IOException {
        if (outputStream != null) {
            File file = new File(outputFile);
            if (!file.exists())
                file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(((ByteArrayOutputStream) outputStream).toByteArray());
            fos.flush();
            fos.close();

            outputStream.close();
        }

        if (inputStream != null)
            inputStream.close();
    }

    @Test
    public void ShouldConvertSourceToDestination() throws IOException {
        inputStream = getSourceFile();
        runTest();
    }

    private void runTest() throws IOException {
        tiffToPdfConverter.convert(inputStream, outputStream);
        //tiffToPdfConverter.readTiffImageProperties(sourceFile);
    }

    protected InputStream getSourceFile() throws FileNotFoundException {
        return new FileInputStream(sourceFile);
    }

}
