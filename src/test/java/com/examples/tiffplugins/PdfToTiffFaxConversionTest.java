package com.examples.tiffplugins;

import java.io.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Created by Mike.Oakley on 6/9/2015.
 */
@RunWith(Parameterized.class)
public class PdfToTiffFaxConversionTest {
    @Parameterized.Parameters
    public static Collection sourceFiles() {
        return Arrays.asList(new Object[][]{
                {new PdfToTiffFaxConverter()}
        });
    }

    protected String sourceFile;
    protected String outputFile;
    protected PdfToTiffFaxConverter pdfToTiffFaxConverter;
    protected OutputStream outputStream;
    protected InputStream inputStream;

    public PdfToTiffFaxConversionTest(PdfToTiffFaxConverter converter)
    {
        String url = PdfToTiffFaxConversionTest.class.getResource("/test/image/").getPath();
        this.sourceFile = url + "ColorSheets23.pdf";
        this.outputFile = url + "ColorSheets23Out.tif";
//        this.sourceFile = url + "FaxTest.pdf";
//        this.outputFile = url + "FaxTestOut.tif";
        this.pdfToTiffFaxConverter = converter;
    }

    @Before
    public void setup() throws FileNotFoundException {
        outputStream = new ByteArrayOutputStream();
    }

    @After
    public void tearDown() throws IOException {
        if (outputStream != null) {
            File file = new File(outputFile);
            if (!file.exists())
                file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(((ByteArrayOutputStream) outputStream).toByteArray());
            fos.flush();
            fos.close();

            outputStream.close();
        }

        if (inputStream != null)
            inputStream.close();
    }

    @Test
    public void ShouldConvertSourceToDestination() throws IOException {
        inputStream = getSourceFile();
        runTest();
    }

    private void runTest() throws IOException {
        pdfToTiffFaxConverter.convert(inputStream, outputStream);
        //pdfToTiffFaxConverter.convert(sourceFile, outputFile);
    }

    protected InputStream getSourceFile() throws FileNotFoundException {
        return new FileInputStream(sourceFile);
    }
}
